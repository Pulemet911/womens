from django.views.decorators.cache import cache_page
from django.urls import path
from women.views import WomenList, about, AddPost, ContactFormView, DetailPost, WomenCategoryList, PostDeleteView,\
    RegisterUser, LoginUser, logout_user, About

urlpatterns = [
    # path('', cache_page(60)(WomenList.as_view()), name='home'),
    path('', WomenList.as_view(), name='home'),
    path('about/', About.as_view(), name='about'),
    path('addpage/', AddPost.as_view(), name='add_page'),
    path('contact/', ContactFormView.as_view(), name='contact'),
    path('login/', LoginUser.as_view(), name='login'),
    path('logout/', logout_user, name='logout'),
    path('register/', RegisterUser.as_view(), name='register'),
    path('post/<slug:post_slug>/', DetailPost.as_view(), name='post'),
    path('category/<slug:cat_slug>/', WomenCategoryList.as_view(), name='category'),
    path('post/<slug:post_slug>/post_delete', PostDeleteView.as_view(), name='post_delete')
]
